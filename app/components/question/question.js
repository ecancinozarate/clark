import Component from '@glimmer/component';

export default class QuestionComponent extends Component {
    get question() {
        return this.args.question;
    }

    get multiple() {
        return this.question.multiple === "true"
    }

    get multipleChoice() {
        return this.question.question_type === "multiple-choice";
    }

    get multiline() {
        return this.question.multiline === "true";
    }
}
