import Route from '@ember/routing/route';
// import { inject as service } from '@ember/service';

export default class IndexRoute extends Route {
  // @service store;

  async model() {
    // return this.store.findAll('questionnaire');

    return fetch('/api/questionnaire.json')
      .then(response => response.json())
      .then(({ questionnaire }) => questionnaire)
  }
}