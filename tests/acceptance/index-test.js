import { module, test } from 'qunit';
import { click, visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | index', function (hooks) {
  setupApplicationTest(hooks);

  test('visiting /', async function (assert) {
    await visit('/');

    assert.equal(currentURL(), '/');
    assert.dom('header').exists();
    assert.dom('.clark-logo').exists();
    assert.dom('h1').hasText('Fragebogen');

    assert.dom('.footer #vorig').hasText('Vorig').isDisabled();
    assert.dom('.footer #nachste').hasText('Nächste').isEnabled();

    await click('.footer #nachste');

    assert.dom('.footer #vorig').isEnabled();
  });
});
