import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import move from 'ember-animated/motions/move'
import { fadeOut, fadeIn } from 'ember-animated/motions/opacity'
import { easeOut, easeIn } from 'ember-animated/easings/cosine'

export default class QuestionnaireComponent extends Component {
  @tracked current = 0;

  /* eslint-disable require-yield */
  * transition({ insertedSprites, removedSprites }) {
    insertedSprites.forEach(sprite => {
      sprite.startAtPixel({ x: 0 })
      move(sprite, { easing: easeIn })
      fadeIn(sprite)
    });

    removedSprites.forEach(sprite => {
      sprite.endAtPixel({ x: window.innerWidth })
      move(sprite, { easing: easeOut })
      fadeOut(sprite)
    });
  }

  get count() {
    return this.current + 1;
  }

  get questions() {
    return this.args?.questionnaire?.questions;
  }

  get question() {
    return this.questions[this.current];
  }

  get last() {
    return this.count === this.questions.length;
  }

  get first() {
    return this.count === 1;
  }

  get stop() {
    return this.last || !this.answered;
  }

  @action nextQuestion() {
    this.current = this.current + 1;
  }

  @action previousQuestion() {
    this.current = this.current - 1;
  }

  get answered() {
    return !this.question.required || this.question.value
  }

  setSelected(question, options) {
    const option = options[options.selectedIndex]
    question.choices = question.choices.map(choice => ({ ...choice, selected: (choice.value === option.value) }))
  }

  @action setValue(event) {
    if (this.question.question_type === "multiple-choice") {
      this.setSelected(this.question, event.target.options)
    }
    this.question.value = event.target.value
  }
}
