import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | select', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders the content for a select form component', async function (assert) {
    const choices = [
      { label: 'First', value: 'one', selected: false },
      { label: 'Second', value: 'two', selected: false },
      { label: 'Third', value: 'three', selected: true }
    ]

    this.set('choices', choices);

    await render(hbs`<Select class="custom-select" @choices={{this.choices}} />`);

    assert.dom('.custom-select').exists();

    this.element.querySelectorAll('option[value]').forEach((option, index) => {
      assert.equal(option.textContent, choices[index].label, `Label ${index}`);
      assert.equal(option.value, choices[index].value, `Value ${index}`);
      assert.equal(option.selected, choices[index].selected, `Selected ${index}`);
    })
  });
});
