# The Insurance Manager

FL Fintech E GmbH | VilbelerStr. 29, 60313 Frankfurt am Main | [http://www.clark.de](http://www.clark.de)

Geschäftsführer: Dr.Christopher Oster, Steffen Glomb, Dr.Marco Adelt

## Coding Challenge Questionnaire

Create a small questionnaire Web Application similar to what [Typeform.com](https://www.typeform.com/#home-examples) offers.

### Concepts

- Questionnaire
  - A Questionnaire has a title and a description
  - A questionnaire has a defined sequence of question that need to be answered
- Question
  - 3 different types of questions are supported: Freetext, Multiple choice (single answer), Multiple choice (multi answer)
  - For Multiple Choice, each choice has a descriptive label that is shown to the user

### Requirements

- As a user I can answers questions very swiftly so that I don’t feel like I am wasting my time
- Your questionnaire is based on a JSON that the frontend uses to drive the questions
- As a user I can go back to a previous question without losing the answers I have given in a current question
- Mobile First, use some SVG, use JS ES 6-7 features, good conventions for CSS

### Task

- Write a small web application that enables a user to answer a questionnaire
- Use the given questionnaire.jsonto derive the definition of a questionnaire
- Use a modern client side MVC Framework like Ember (highly preferred), GlimmerVM(bleeding edge), or React (although we prefer ember J)

### Goals/Priorities

- Show UX skills, use CSS and possibly animate the transition between questions (high priority)
- At least 1 component test (not auto generated) (high priority)
- Clean code and MVC based application structure (high priority)
- There is no need to store results in a database
- Bonus: The design is responsive

### General remarks

Please provide the code through a private Githubor Bitbucketrepository. If you don’t have an account to a repo we can provide one. Please keep track of the time you spent on the tasks.

We are looking forward to discussing the result with you!
